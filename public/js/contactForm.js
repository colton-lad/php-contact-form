function validateForm() {
	
	// Validate the information the user gave us.
	// Taking advantage of shortened syntax with JQuery!
	// This is purely for user experience.  Should never rely on data validation client-side.
	// When validated, change to base64 using btoa for safe POSTing.
	
    var regex;
    var firstName = $('#firstName').val();
    if (firstName == "") {
        $('#status').html("First Name cannot be empty");
        return false;
    }
    firstName = btoa(firstName);

    var lastName = $('#lastName').val();
    if (lastName == "") {
        $('#status').html("Last Name cannot be empty");
        return false;
    }
    lastName = btoa(lastName);

    var email = $('#email').val();
    if (email == "") {
        $('#status').html("Email Address cannot be empty");
        return false;
    } else {
        regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!regex.test(email)) {
            $('#status').html("Email Address format invalid");
            return false;
        }
    }
    email = btoa(email);

    var phone = $('#phone').val();
    if (phone != "") {
        regex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
        if (!regex.test(phone)) {
            $('#status').html("Phone Number format invalid");
            return false;
        }
        phone = btoa(phone);
    }

    var message = $('#message').val();
    if (message == "") {
        $('#status').html("Message cannot be empty");
        return false;
    }
    message = btoa(message);

    $('#status').html("Sending...");

    var formData = {
        "firstName" : firstName,
        "lastName" : lastName,
        "email" : email,
        "phone" : phone,
        "message" : message
    };
	
	// Pass our data object to our ajax function to send to a server-side PHP file for processing.

    sendAJAX(formData, "php/procContactForm.php");

}

// Send our object through an ajax request to procContactForm.php.
// Allows seamless updates and a better experience for the user.
// And less files server-side :)

function sendAJAX(data, sendTo) {
	$.ajax({
		"url" : sendTo,
		"type" : "POST",
		"data" : data,
		
		// If request was successfully sent, handle return message.
		// Inform user of outcome.
		"success" : function(recData, textStatus, jqXHR){
			data = JSON.parse(atob(recData));
			switch(data.status){
				case '0':
					$('#status').text("Message sent.  We'll contact you soon!");
					break;
				default:
					$('#status').text(data.message);
					break;
			}
			
		},
		
		// If request was not successfully sent, inform user.
		"error" : function(jqXHR, textStatus, error){
			$('#status').text(error);
		}
	});
}
