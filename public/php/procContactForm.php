<?php

	// Constants at top for easy access and change
	define("TO", "guy-smiley@example.com");
	define("SUBJECT", "Contact Form");
	define("HOME", "../");
	define("DB", "dealInspire");
	define("COLL", "contactEmails");
	
	// Get our Email class
	require '../../phpIncludes/email.php';
	
	$email = new Email();
	
	// Check if our $_POST vars exist and decode them from out base64 encoding done client-side
	// Then setup our email object
	// If our required POST vars are not present, send them to the home screen
	
	if(isset($_POST['firstName']) && isset($_POST['lastName'])){
		$email->setName(base64_decode($_POST['firstName']).' '.base64_decode($_POST['lastName']));
	}
	else sendHome();
	
	if(isset($_POST['email'])){
		$email->setFrom(base64_decode($_POST['email']));
	}
	else sendHome();
	
	if(isset($_POST['phone'])){
		$email->setPhone(base64_decode($_POST['phone']));
	}
	
	if(isset($_POST['message'])){
		$email->setMessage(base64_decode($_POST['message']));
	}
	else sendHome();
	
	$email->setTo(TO);
	$email->setSubject(SUBJECT);
	
	// sendEmail will validate our data first
	// If anything is invalid, will return false
	
	
	if(!$email->sendEmail()){
		handleError(1);
	}
	
	// Try to store our email in MongoDB
	// If it could not insert into collection, will return false
	
	if(!$email->storeEmail(DB, COLL)){
		handleError(2);
	}
	
	// Nothing went wrong!  Return status of 0 to our ajax client
	
	print base64_encode(json_encode(array('status' => '0')));
	exit();
	
	// Something went wrong!  Pass the error to our ajax client
	// Could also setup a save to log collection in MongoDB here too...
	
	function handleError($error){
		switch($error){
			case 1:
				print base64_encode(json_encode(array('message' => "Email failed to send!", 'status' => '1')));
				exit();
				break;
			case 2:
				print base64_encode(json_encode(array('message' => "Email sent but not stored!", 'status' => '1')));
				exit();
				break;
		}
	}
	
	// Sends the client to the base dir through header editing
	
	function sendHome(){
		header('Location: '.HOME);
	}

?>