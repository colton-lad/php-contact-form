<?php

require_once __DIR__.'/../phpIncludes/email.php';
use PHPUnit\Framework\TestCase;
 
class EmailTests extends TestCase
{
    private $email;
 
		//Create new Email Object before each test
    protected function setUp(): void
    {
        $this->email = new Email();
    }
 
		//Set email to null after each test
    protected function tearDown(): void
    {
        $this->email = NULL;
    }
 
		//All Valid inputs with phone being 11 numbers
    public function testSendAllValidMax(){
        $this->email->setName('John Doe');
		$this->email->setTo('to@some.com');
		$this->email->setFrom('from@some.com');
		$this->email->setPhone('12223334444');
		$this->email->setSubject('Some Subject');
		$this->email->setMessage('Some message here!');
		$this->assertEquals(true, $this->email->isValid());
    }
		//All Valid inputs with phone being 10 numbers
	public function testSendAllValidMin(){
		$this->email->setName('John Doe');
		$this->email->setTo('to@some.com');
		$this->email->setFrom('from@some.com');
		$this->email->setPhone('2223334444');
		$this->email->setSubject('Some Subject');
		$this->email->setMessage('Some message here!');
		$this->assertEquals(true, $this->email->isValid());
	}
	
		//All Valid inputs with no phone
	public function testSendAllValidNoPhone(){
		$this->email->setName('John Doe');
		$this->email->setTo('to@some.com');
		$this->email->setFrom('from@some.com');
		$this->email->setSubject('Some Subject');
		$this->email->setMessage('Some message here!');
		$this->assertEquals(true, $this->email->isValid());
	}
	
		//All valid, but no name
	public function testSendNoName(){
		$this->email->setTo('to@some.com');
		$this->email->setFrom('from@some.com');
		$this->email->setPhone('12223334444');
		$this->email->setSubject('Some Subject');
		$this->email->setMessage('Some message here!');
		$this->assertEquals(false, $this->email->isValid());
		$this->assertArrayHasKey('name', $this->email->getErrors(), implode(' ', $this->email->getErrors()));
	}
	
		//All valid, but no to
	public function testSendNoTo(){
		$this->email->setName('John Doe');
		// $this->email->setTo('to@some.com');
		$this->email->setFrom('from@some.com');
		$this->email->setPhone('12223334444');
		$this->email->setSubject('Some Subject');
		$this->email->setMessage('Some message here!');
		$this->assertEquals(false, $this->email->isValid());
		$this->assertArrayHasKey('to', $this->email->getErrors());
	}
	
		//All valid, but no from
	public function testSendNoFrom(){
		$this->email->setName('John Doe');
		$this->email->setTo('to@some.com');
		// $this->email->setFrom('from@some.com');
		$this->email->setPhone('12223334444');
		$this->email->setSubject('Some Subject');
		$this->email->setMessage('Some message here!');
		$this->assertEquals(false, $this->email->isValid());
		$this->assertArrayHasKey('from', $this->email->getErrors());
	}
	
		//All valid, but no subject
	public function testSendNoSubject(){
		$this->email->setName('John Doe');
		$this->email->setTo('to@some.com');
		$this->email->setFrom('from@some.com');
		$this->email->setPhone('12223334444');
		// $this->email->setSubject('Some Subject');
		$this->email->setMessage('Some message here!');
		$this->assertEquals(false, $this->email->isValid());
		$this->assertArrayHasKey('subject', $this->email->getErrors());
	}
	
		//All valid, but no message
	public function testSendNoMessage(){
		$this->email->setName('John Doe');
		$this->email->setTo('to@some.com');
		$this->email->setFrom('from@some.com');
		$this->email->setPhone('12223334444');
		$this->email->setSubject('Some Subject');
		// $this->email->setMessage('Some message here!');
		$this->assertEquals(false, $this->email->isValid());
		$this->assertArrayHasKey('message', $this->email->getErrors());
	}
		//Invalid to
	public function testSendInvalidTo(){
        $this->email->setName('John Doe');
		$this->email->setTo('tosome.com');
		$this->email->setFrom('from@some.com');
		$this->email->setPhone('12223334444');
		$this->email->setSubject('Some Subject');
		$this->email->setMessage('Some message here!');
		$this->assertEquals(false, $this->email->isValid());
		$this->assertArrayHasKey('to', $this->email->getErrors());
    }
	
		//Invalid from
	public function testSendInvalidFrom(){
        $this->email->setName('John Doe');
		$this->email->setTo('to@some.com');
		$this->email->setFrom('fromsome.com');
		$this->email->setPhone('12223334444');
		$this->email->setSubject('Some Subject');
		$this->email->setMessage('Some message here!');
		$this->assertEquals(false, $this->email->isValid());
		$this->assertArrayHasKey('from', $this->email->getErrors());
    }
	
	//Invalid phone too few numbers
	public function testSendInvalidPhoneFew(){
        $this->email->setName('John Doe');
		$this->email->setTo('to@some.com');
		$this->email->setFrom('from@some.com');
		$this->email->setPhone('1222333');
		$this->email->setSubject('Some Subject');
		$this->email->setMessage('Some message here!');
		$this->assertEquals(false, $this->email->isValid());
		$this->assertArrayHasKey('phone', $this->email->getErrors());
    }
	
	//Invalid phone with not just numbers
	public function testSendInvalidPhone(){
        $this->email->setName('John Doe');
		$this->email->setTo('to@some.com');
		$this->email->setFrom('from@some.com');
		$this->email->setPhone('222a334444');
		$this->email->setSubject('Some Subject');
		$this->email->setMessage('Some message here!');
		$this->assertEquals(false, $this->email->isValid());
		$this->assertArrayHasKey('phone', $this->email->getErrors());
    }
	
		//Test toArray function
	public function testToArray(){
		$this->email->setName('John Doe');
		$this->email->setTo('to@some.com');
		$this->email->setFrom('from@some.com');
		$this->email->setPhone('12223334444');
		$this->email->setSubject('Some Subject');
		$this->email->setMessage('Some message here!');
		
		$emailArray = array(
			'name' => 'John Doe',
			'to' => 'to@some.com',
			'from' => 'from@some.com',
			'phone' => '12223334444',
			'subject' => 'Some Subject',
			'message' => 'Some message here!'
		);
		
		$this->assertEquals($emailArray, $this->email->toArray());
	}
 
}

?>