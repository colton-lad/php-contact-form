<?php

//Better as a class so we can make use of it in multiple places

class Email {
	private $name;
	private $to;
	private $from;
	private $phone;
	private $subject;
	private $message;
	private $errors;
	
	function __construct(){
		$this->errors = array();
	}
	
	// SETTERS and GETTERS------------------------------------------
	
	function setName($name){
		$this->name = $name;
	}
	
	function getName(){
		return $this->name;
	}
	
	function setTo($to){
		$this->to = filter_var($to, FILTER_SANITIZE_EMAIL);
	}
	
	function getTo(){
		return $this->to;
	}
	
	function setFrom($from){
		$this->from = filter_var($from, FILTER_SANITIZE_EMAIL);
	}
	
	function getFrom(){
		return $this->from;
	}
	
	function setPhone($phone){
		$this->phone = preg_replace('/[^0-9]/', '', $phone);
	}
	
	function getPhone(){
		return $this->phone;
	}
	
	function setMessage($message){
		$this->message = $message;
	}
	
	function getMessage(){
		return $this->message;
	}
	
	function setSubject($subject){
		$this->subject = $subject;
	}
	
	function getSubject(){
		return $this->subject;
	}
	
	function getErrors(){
		return $this->errors;
	}
	
	// Functional methods-----------------------------------
	
	function isValid(){
		if($this->name == ''){
			$this->errors = array("name" => "missing");
			return false;
		}
		if($this->to == ''){
			$this->errors = array("to" => "missing");
			return false;
		}
		if($this->from == ''){
			$this->errors = array("from" => "missing");
			return false;
		}
		if($this->subject == ''){
			$this->errors = array("subject" => "missing");
			return false;
		}
		if($this->message == ''){
			$this->errors = array("message" => "missing");
			return false;
		}
		if(!filter_var($this->to, FILTER_VALIDATE_EMAIL)){
			$this->errors = array("to" => "invalid");
			return false;
		}
		if(!filter_var($this->from, FILTER_VALIDATE_EMAIL)){
			array_push($this->errors, 'from', 'invalid');
			$this->errors = array("from" => "invalid");
			return false;
		}
		if($this->phone != '' && strlen($this->phone) !== 10 && strlen($this->phone) !== 11){
			$this->errors = array("phone" => "invalid");
			return false;
		}
		
		return true;
	}
	
	function sendEmail(){
		if(!$this->isValid()){
			return false;
		}
		
		$content = "From:\n\n$this->name\n$this->from\n\n$this->message";
		$mailHeader = "From: $this->from \r\n";
		if(mail($this->to, $this->subject, $content, $mailHeader)){
			return true;
		}
		return false;
	}
	
	function toArray(){
		return array(
			'name' => $this->name,
			'to' => $this->to,
			'from' => $this->from,
			'phone' => $this->phone,
			'subject' => $this->subject,
			'message' => $this->message
		);
	}
	
	function storeEmail($db, $collection){
		//Require the DB form.  Gives access to $conn already connected.  Hides sensitive info.
		require_once __DIR__.'/../db/db.php';
		$coll = $conn->$db->$collection;
		if($coll->insertOne($this->toArray())){
			return true;
		}
		return false;
	}
}
?>